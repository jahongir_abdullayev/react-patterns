import './App.css';
import { LoginPage } from './components/loginPage/login_page';

function App() {
  return (
    <div className="App">
      <LoginPage />
    </div>
  );
}

export default App;
