import React from "react";
import { Button, FormControl, Input, InputAdornment, InputLabel, TextField } from "@material-ui/core";
import { AccountCircle, Lock, Phone } from "@material-ui/icons";
import cls from './login_page.module.css'


export const LoginPage = () => {
    return (
      <div className={cls.wrapper}>
        <div className={cls.first_box}>
          <div className={cls.logo}>UDEVS</div>
        </div>
        <div className={cls.second_box}>
          <div className={cls.login_box}>
            <div className={cls.login_header}>Войти в систему</div>
            <div className={cls.input_field}>
              <FormControl variant="standard"></FormControl>
              <div className={cls.input_text}>Имя пользователья</div>
              <Input
                id="input-with-icon-adornment"
                // variant="outlined"
                placeholder="Введите имя пользователья"
                className={cls.textField}
                startAdornment={
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                }
              />
            </div>

            <div className={cls.input_field}>
              <FormControl variant="standard"></FormControl>
              <div className={cls.input_text}>Пароль</div>
              <Input
                id="input-with-icon-adornment"
                // variant="outlined"
                placeholder="Введите Пароль"
                className={cls.textField}
                startAdornment={
                  <InputAdornment position="start">
                    <Lock />
                  </InputAdornment>
                }
              />
            </div>

            <Button
              className={cls.enter_button}
              type="submit"
              color="blue"
              shape="filled"
              size="large"
            >
              Войти
            </Button>
          </div>

          <div className={cls.service_box}>
            <div className={cls.contact_info}>
              <span className={cls.contact}>
                <Phone color="primary" className={cls.phone}/>
                <span style={{color : '#84919A'}}>Служба поддержки</span>
              </span>
              <span>+998 (90) 123-45-67</span>
            </div>
          </div>
        </div>
      </div>
    );
}